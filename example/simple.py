import irida_bot

#Настройка бота

bot = irida_bot.IridaBot(host = 'https://irida-vega.ru/irida', userID = '1927', apikey = '1491da9e-10a6-41f2-9566-a9ba5d061fdd')

# Функция на комманду /help
def help_answer(data):
    print('Help answer callback')
    bot.send_letter(data['thread_id'], 'List command: <br> /hello <br> /help <br> /irida')

bot.add_handler(command = 'help', callback = help_answer)

# Функция на комманду /hello
def hello_answer(data):
    print('Hello answer callback')
    bot.send_letter(data['thread_id'], 'Hello, ' + data['sender_tag'] + '!')

bot.add_handler(command = 'hello', callback = hello_answer)

# Функция на комманду /irida
def irida_answer(data):
    print('Irida answer callback')
    info = bot.request('/api/info')
    bot.send_letter(data['thread_id'], info['version'])

bot.add_handler(command = 'irida', callback = irida_answer)

# Функция на отсутствующую команду
def default_answer(data):
    print('Default message callback')
    bot.send_letter(data['thread_id'], 'Введите /help, чтобы узнать доступные команды')

bot.add_handler(command = irida_bot.WRONG_COMMAND, callback = default_answer)

# Функция на любое текстовое сообщение
def any_answer(data):
    print('Any message callback')
    text = data['text'].split('<br>')[0]
    bot.send_letter(data['thread_id'], text[::-1])

bot.add_handler(command = irida_bot.ANY_MESSAGE, callback = any_answer)

def start_bot(data):
    print('Start callback')
    bot.update_auth()
    bot.send_message_broadcast('Bot enabled')
    
def end_bot():
    print('End callback')
    bot.update_auth()
    bot.send_message_broadcast('Bot disabled')

# Старт бота
bot.start_polling()
bot.idle(start_bot, end_bot)