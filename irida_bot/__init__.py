"""
Модуль для создания ботов на irida
"""

import requests
import websocket
import time
import ssl
import random
import string
import json

try:
    import thread
except ImportError:
    import _thread as thread

DEFAULT_HOST = 'http://localhost:8000'
DEFAULT_USER_ID = '2020'
DEFAULT_API_KEY = 'pasha'

ANY_MESSAGE = '__any'
WRONG_COMMAND = '__cancel'

class IridaBot:
    sessionID = ''
    handleID = ''
    callbackDict = {}
    callbackAny = []
    callbackStart = ''
    callbackEnd = ''
    transactionPull = {}


    def __init__(self, host = DEFAULT_HOST, userID = DEFAULT_USER_ID, apikey = DEFAULT_API_KEY):
        self.host = host
        self.userID = str(userID)
        self.apikey = apikey
        self.sessionID = ''

    def request(self, endpoint = '/', type = 'GET', data = {}, json = False):
        type = type.upper()
        url = self.host + endpoint

        try:
            if (type == 'GET'):
                response = requests.get(url, params=data)

            elif (type == 'POST'):
                if json:
                    response = requests.post(url, json=data)
                else:
                    response = requests.post(url, data=data)

            elif (type == 'PUT'):
                response = requests.put(url, data=data)

            elif (type == 'DELETE'):
                response = requests.delete(url, data=data)

            else:
                response = requests.get(url, params=data)

        except requests.exceptions.ReadTimeout:
            self.loger('REQUEST:('+url+') Read timeout occured')

        except requests.exceptions.ConnectTimeout:
            self.loger('REQUEST:('+url+') Connection timeout occured!')

        except requests.exceptions.InvalidSchema:
            self.loger('REQUEST:('+url+') invalid schema')

        result = False

        try:
            result = response.json()
        except:
            self.loger('REQUEST:('+url+') RESPONSE IS NOT JSON')
        
        self.loger(result)

        if ('error' in result):
            self.loger('REQUEST:('+url+') ERROR RESPONSE:')
            self.loger(result['error'])

        return result
    
    def send_letter(self, threadID, text, callback = ''):
        self.__send_janus({
            "janus":"message",
            "body":{
                "request":"sendletter",
                "mode":"multicast",
                "thread_id":int(threadID),
                "text":str(text),
                "sender_id": str(self.userID),
                "preview_text":"",
                "preview_data":"",
                "type":"message",
                "severity":"regular",
                "sender_tag":"PYTHON BOT"
            },
            "session_id":int(self.sessionID),
            "handle_id": int(self.handleID)
        }, callback)

    def get_available_threads(self):
        threads = self.request('/api/threads', data={'apikey' : self.apikey, 'id' : self.userID})

        return threads

    def send_message_broadcast(self, text):
        threads = self.get_available_threads()
        for thread in threads['threads']:
            self.send_message_to_thread(thread['id'], text)

    def send_letter_broadcast(self, text, callback = ''):
        threads = self.get_available_threads()
        for thread in threads['threads']:
            self.send_letter(thread['id'], text, callback)

    def add_handler(self, command, callback):
        if command == '__any':
            self.callbackAny.append(callback)
        else:
            self.callbackDict[command] = callback

    def update_auth(self):
        response = self.request('/api/auth', data = {'apikey' : self.apikey, 'id' : self.userID})

        if ('token' in response):
            self.token = response['token']
        else:
            self.loger('AUTH FAILED')
        
    def start_polling(self):
        info = self.request('/api/info')
        self.ws_url = info['janus']

        websocket.enableTrace(True)
        self.websocket = websocket.WebSocketApp(self.ws_url,
                                on_message = self.__on_message,
                                on_error = self.__on_error,
                                on_close = self.__on_close,
                                on_open = self.__on_open,
                                subprotocols=["janus-protocol"])
        
    def idle(self, callback_start = '', callback_end = ''):
        if (callback_start != ''):
            self.callbackStart = callback_start
        if (callback_end != ''):
            self.callbackEnd = callback_end
        self.websocket.run_forever(sslopt={"check_hostname": False})

    def send_message_to_user(self, userID, text):
        response = self.request('/api/im/sendmessagetousers?apikey='+self.apikey, type='post', data={"message": text, "sender_id": int(self.userID), "users": [ int(userID) ] }, json=True)
        return response

    def send_message_to_thread(self, threadID, text):
        response = self.request('/api/im/sendmessagetothread?token='+self.token+'&thread_id='+threadID, type="post", data={"message" : text}, json = True)
        return response
    
    def __set_sessionID(self, json_data):
        self.sessionID = str(json_data['data']['id'])
        self.loger('Transaction set. Session ID:' + self.sessionID)

        self.__send_janus({
            "janus":"attach",
            "plugin":"janus.plugin.recordplay",
            "session_id": int(self.sessionID)
        }, self.__set_handleID)

    def __set_handleID(self, json_data):
        self.handleID = str(json_data['data']['id'])
        self.loger('Handle set. Handle ID:' + self.handleID)

        self.__send_janus({
            "janus":"message",
            "body":{
                "request":"join",
                "client_id": str(self.userID)
                },
            "session_id": int(self.sessionID),
            "handle_id": int(self.handleID)
        }, self.callbackStart)

    def __on_message(self, message):
        print('### message ###')
        print(message)
        json_data = json.loads(message)

        if 'transaction' in json_data and json_data['transaction'] in self.transactionPull:
            self.transactionPull[json_data['transaction']](json_data)
            self.transactionPull.pop(json_data['transaction'],None)

        if json_data['janus'] == 'event':
            if 'plugindata' in json_data:
                if 'data' in json_data['plugindata']:
                    if 'incoming_letter' in json_data['plugindata']['data']:
                        data = json_data['plugindata']['data']
                        if data['sender_id'] != self.userID:
                            command = self.__check_command(data['text'])
                            if command:
                                if self.callbackDict.get(command, False):
                                    self.callbackDict.get(command)(data=data)
                                elif (self.callbackDict.get('__cancel', False)):
                                    self.callbackDict.get('__cancel')(data=data)
                            else:
                                for callback in self.callbackAny:
                                    callback(data=data)

    def __on_error(self, error):
        print(error)

    def __on_close(self):
        print("### closed ###")
        if self.callbackEnd != '':
            self.callbackEnd()

    def __on_open(self):
        print('### open ###')
        self.__send_janus({
            "janus":"create",
        }, self.__set_sessionID)
        def run(*args):
            while(True):
                time.sleep(30)
                if (self.sessionID != '' and self.handleID != ''):
                    self.__send_janus({
                        "janus":"keepalive",
                        "session_id": int(self.sessionID),"transaction": str(self.transaction())})

        thread.start_new_thread(run, ())

    def __check_command(self, text):
        #todo
        text = text.split('<br>')[0]
        if text[0] == '/':
            return text[1:]
        else:
            return False

    #not a typo
    def loger(self, data):
        print(data)

    def __transaction(self):
        return ''.join([random.choice(string.ascii_letters + string.digits) for n in range(8)])

    def __send_janus(self, data, callback = ''):
        transaction = self.__transaction()
        data['transaction'] = transaction

        if (callback != ''):
            self.transactionPull[transaction] = callback 

        json_data = json.dumps(data)
        self.websocket.send(json_data)
