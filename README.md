## Модуль irida_bot
### Инструкия к написанию бота для Irida
#### 1. Установка модуля

```bash
git clone https://gitlab.com/vega_spb/irida_bot.git
cd irida_bot
python setup.py install
```

#### 2. Необходимые данные для инициализации
Перед стартом нужно иметь api-ключ доступа и ID пользователя, который станет ботом. Для это нужно зарегистрировать нового пользователя, перейти в его профиле на вкладку *"Доступ"* и скопировать от туда *"Идентификатор пользователя"*.
#### 3. Использование модуля
Пример простого скрипта для ответа на любое сообщение:
```python
import irida_bot

#Настройка бота
bot = irida_bot.IridaBot(host = "https://irida.online/irida", userID = "%ID бота%", apikey = "%ключ доступа%")

# Функция ответа на любое текстовое сообщение
def any_answer(data):
	print('Any message callback')
	text = data['text'].split('<br>')[0]
 	#Переворачиваем сообщение и отправляем его обратно
	bot.send_letter(data['thread_id'], text[::-1])

#Вешаем функция на прослушивание всех сообщение
bot.add_handler(command = irida_bot.ANY_MESSAGE, callback = any_answer)

# Старт бота
bot.start_polling()
bot.idle()
```
После запуска бот будет работать без остановки. 

#### 4. Описание методов
`update_auth()` - обновляет токен доступа (доступен через поле `token`). *Необходим для использование API-методов.*

```python
bot.update_auth() 
#>> {'status': 'success', 'token': '7d5a096f5e2549f5ab51417f303a9269'}
print(bot.token) 
#>> 7d5a096f5e2549f5ab51417f303a9269
```

`get_available_threads()` - возвращает список доступных боту тредов.

`send_message_to_user(userID, text)` - отправляет сообщение пользователю через API.

`send_message_to_thread(threadID, text)` - отправляет сообщение в тред через API.

`send_message_broadcast(text)` - отправляет сообщение всем доступным тредам через API.

`add_handler(command, callback)` - добавляет обработчик на команду вида */%название_команды%*, где %название_команды% задается аргументом *command*. Так же доступны универсальные события: 
**ANY_MESSAGE** - обработчик на любое сообщение;
**WRONG_COMMAND** - обработчик на неверную команду;

```python
# Функция на комманду /hello
def hello_answer(data):
    print('Hello answer callback')
    bot.send_message_to_thread(data['thread_id'], 'Hello, ' + data['sender_tag'] + '!')

bot.add_handler(command = 'hello', callback = hello_answer)

# Функция на отсутствующую команду
def default_answer(data):
    print('Default message callback')
    bot.send_message_to_thread(data['thread_id'], 'Неверная команда. Введите /hello для начала работы.')

bot.add_handler(command = irida_bot.WRONG_COMMAND, callback = default_answer)

# Функция на любое текстовое сообщение
def any_answer(data):
    print('Any message callback')
    text = data['text'].split('<br>')[0]
    bot.send_message_to_thread(data['thread_id'], text[::-1])

bot.add_handler(command = irida_bot.ANY_MESSAGE, callback = any_answer)

```

`start_polling()` - инициализация подключения к Janus.

`idle([calback_start, [calback_end]])` - запуск прослушивани обработчиков через Janus, которые были заданы методом add_handler. Есть возможность задать коллбэки на старт прослушивания, который выполнится сразу же после того, как будет одобрено подключение к сессии, и на конец работы (например, обрыв связи или отключения бота вручную). Вызывать метод необходимо в связке с `start_polling()`

```python
def start_bot(data):
    print('Start callback')
    bot.update_auth()
    bot.send_letter_broadcast('Bot enabled')
    
def end_bot():
    print('End callback')
    bot.update_auth()
    bot.send_message_broadcast('Bot disabled')

# Старт бота
bot.start_polling()
bot.idle(start_bot, end_bot)
```

`send_letter(threadID, text[, callback])` - отправляет сообщение в тред через Janus. Отличие от `send_message_to_thread()` в скорости отправки и в возможности назначить коллбэк на ответ Janus'а. Работает только после соединение через `idle()`.

`send_letter_broadcast(text[, callback])` - отправляет сообщение всем доступным тредам через Janus. Отличие от `send_message_broadcast()` в скорости отправки и в возможности назначить коллбэк на каждый ответ Janus'а. Работает только после соединение через `idle()`.



