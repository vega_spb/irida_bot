import setuptools

with open("README.md", "r", encoding = "utf-8") as fh:
    long_description = fh.read()

setuptools.setup(
    name="irida_bot-vega", # Replace with your own username
    version="1.0.0",
    author="Vega",
    author_email="irida@spb.vega.su",
    description="Module for create irida-bot",
    long_description=long_description,
    long_description_content_type="text/markdown",
    url="https://gitlab.com/vega_spb/irida_bot/",
    packages=setuptools.find_packages(),
    classifiers=[
        "Programming Language :: Python :: 3",
        "License :: OSI Approved :: MIT License",
        "Operating System :: OS Independent",
    ],
    python_requires='>=3.0',
)